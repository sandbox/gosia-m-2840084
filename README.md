# Collapsible list

## Overview
Collapsible_list is a developer module.

This module exposes theme_collapsible_list, which will theme an array as 
a collapsible unordered (<ul>) or ordered (<ol>) list.
Only few list items are shown by default. The rest is hidden and a 'View all' 
control is provided after the last visible list item. When selected, all list 
items are shown, followed by button 'View less'.

## Requirements
This module requires jQuery (1.7.2+).

## Usage
### Parameters
$variables: an associative array containing:
* items: an array of items to be displayed in the collapsible list
* title_text: the title of the list (default: '')
* title_wrapper: HTML element used as list title's wrapper, for example <h2> 
(default: 'h2')
* title_path: if list title is meant to be a link, this parameter is used to 
pass the value of link's 'href' attribute (default: '')
* title_options: the options applied to the list title link 
(default: array('attributes'  => array(), 'html'=> false) )
* type: the type of list to return - <ol> or <ul> (default: 'ul')
* cutoff - number of list items which should be visible (default: 3)
* show_text - show/hide control's label when the list is collapsed (default: 
'View all')
* hide_text - show/hide control's label when the list is expanded (default: 
View less')
* wrapper_class - class added to the collapsible-list element (default: 
'collapsible-list')
* control_class - class added to the show/hide button 
(default: 'collapsible-list__control')
* list_class - class added to the list of items 
(default: 'collapsible-list__items')
* list_item_class - class added to each list item 
(default: 'collapsible-list__item')


### Examples
Function theme_collapsible_list accepts a single keyed array as an argument:

theme('collapsible_list', $variables);

In its simplest form, $variables only requires an 'items' array:

```php
$variables = array(
  'items' => array('item1', 'item2', 'item3'),
);
theme('collapsible_list', $variables);
```


By default, all items are displayed as an unordered list. This can be changed to 
ordered list using 'type' variable. Please note that 'type' variable only 
accepts two values: 'ol' or 'ul':

```php
$variables = array(
  'items' => array('item1', 'item2', 'item3'),
  'type' => 'ol',
);
theme('collapsible_list', $variables);
```

An optional 'title_text' key will add a title to the output. By default list 
title will be marked-up as a level two heading (<h2>). It is possible, however, 
to change its mark-up using 'title_wrapper' variable to ensure correct heading 
structure on page:

```php
$variables = array(
  'items' => array('item1', 'item2', 'item3'),
  'title_text' => 'This is the title for my collapsible list',
  'title_wrapper' => 'h4',
);
theme('collapsible_list', $variables);
```

Title can also be a link. In that case, a title_text and title_path variables
need to be passed (with or without an optional title_options variable):

```php
$variables = array(
  'items' => array('item1', 'item2', 'item3'),
  'title_text' => 'This is the title for my collapsible list (link text)',
  'title_path' => 'http://google.com',
  'title_options' => array(
    'attributes'  => array(
      'title'  => 'Go to google.com',
      'class'  => 'link-class',
    ),
   ),
);
theme('collapsible_list', $variables);
```

A further 'attributes' key can optionally be included, which will passed on to
drupal_attributes() to allow custom attributes to be added, such as class:

```php
$variables = array(
  'attributes' => array(
    'class' => 'custom-class',
  ),
  'items' => array('item1', 'item2', 'item3'),
);
theme('collapsible_list', $variables);
```
