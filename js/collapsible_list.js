/*
  * @file
  * Collapsible list
  *
  *
  * @description: custom plugin for displaying a collapsible list of items
  * @author: Gosia Mlynarczyk (goska_m)
  * @author: Fabricio Nascimento (fadonascimento)
  * @license: licensed under MIT - http://opensource.org/licenses/mit-license.php
 */

(function ($) {
  'use strict';
  var CollapsibleList = function (element, options) {
    this.settings = options || {};
    this.$element = $(element);
    this.setOptions()
        .init();
  };

  CollapsibleList.prototype = {

    constructor: CollapsibleList,

    init: function () {
      var button = $('<button class="' + this.options.controlClass + '" aria-controls="' + this.collapsibleListId + '" aria-expanded="false"/>');
      var itemsNumber = this.$element.find('.' + this.options.listItemClass).length;

      button.text(this.options.showText);

      if (itemsNumber > this.options.numberOfItemsToShow && this.options.numberOfItemsToShow !== 0) {
        button.insertAfter(this.$element.find('.' + this.options.listClass));
        this.$element.find('.' + this.options.listItemClass).filter(':eq(' + this.options.numberOfItemsToShow + '), :gt(' + this.options.numberOfItemsToShow + ')').hide();
        this.$element.find('.' + this.options.listItemClass).filter(':eq(' + this.options.numberOfItemsToShow + '), :gt(' + this.options.numberOfItemsToShow + ')').first().addClass(this.options.listItemClass + '--first-hidden').attr('tabindex', '-1').css('outline', 'none');
        this.bind(button);
      }
    },

    bind: function (button) {
      var self = this;
      button.click(self.callbackClickButton.bind(self));
    },

    callbackClickButton: function (e) {

      var $this = $(e.currentTarget);

      if ($this.attr('aria-expanded') === 'false') {
        this.$element.find('.' + this.options.listItemClass).filter(':eq(' + this.options.numberOfItemsToShow + '), :gt(' + this.options.numberOfItemsToShow + ')').show();
        $this.attr('aria-expanded', 'true').text(this.options.hideText);
        this.$element.find('.' + this.options.listItemClass + '--first-hidden').focus();
      }
      else {
        this.$element.find('.' + this.options.listItemClass).filter(':eq(' + this.options.numberOfItemsToShow + '), :gt(' + this.options.numberOfItemsToShow + ')').hide();
        $this.attr('aria-expanded', 'false').text(this.options.showText);
      }
    },

    setOptions: function () {
      var settings;

      this.collapsibleListId = this.$element.attr('id');
      this.settings.collapsible_list = this.settings.collapsible_list || {};

      if (this.settings.collapsible_list[this.collapsibleListId]) {
        var collapsibleListOptions = this.settings.collapsible_list[this.collapsibleListId];
        settings = {
          wrapperClass: collapsibleListOptions.wrapperClass,
          controlClass: collapsibleListOptions.controlClass,
          listClass: collapsibleListOptions.listClass,
          listItemClass: collapsibleListOptions.listItemClass,
          showText: collapsibleListOptions.showText,
          hideText: collapsibleListOptions.hideText,
          numberOfItemsToShow: collapsibleListOptions.numberOfVisibleItems
        };
      }
      this.options = $.extend({}, $.fn.collapsibleList.defaults, settings);
      return this;
    },

    destroy: function () {
      this.$element.find('.' + this.options.controlClass).unbind().remove();
      this.$element.find('.' + this.options.listItemClass).show().removeAttr('style');
      this.$element.find('.' + this.options.listItemClass + '--first-hidden').removeClass(this.options.listItemClass + '--first-hidden').removeAttr('tabindex');
      this.$element.removeData();
    }
  };

  $.fn.collapsibleList = function (options) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('collapsibleList');

      if (!data) {
        $this.data('collapsibleList', (data = new CollapsibleList(this, options)));
      }
      if (typeof options == 'string') {
        data[options]();
      }
    });
  };

  $.fn.collapsibleList.defaults = {
    wrapperClass: 'collapsible-list',
    controlClass: 'collapsible-list__control',
    listClass: 'collapsible_list__items',
    listItemClass: 'collapsible_list__item',
    showText: Drupal.t('View all'),
    hideText: Drupal.t('View less'),
    numberOfItemsToShow: 3
  };

})(jQuery);

(function ($) {
  'use strict';
  Drupal.behaviors.collapsible_list = {
    attach: function (context, settings) {
      $('[data-js="collapsible-list"]', context).collapsibleList(settings);
    },
    detach: function (context, settings) {
      $('[data-js="collapsible-list"]', context).collapsibleList('destroy');
    }
  };
})(jQuery);
